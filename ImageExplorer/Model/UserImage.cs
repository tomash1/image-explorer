﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ImageExplorer.Model
{
    public enum ImageFileType { JPG=0, PNG=1, NOT_SUPPORTED=2 }

    public class UserImage
    {

        #region Fields

        private ImageFileType type;
        private string path;
        private BitmapSource miniature;

        #endregion

        #region Properties

        public ImageFileType Type { get { return type; } }
        public string Path { get { return path;  } }
        public BitmapSource Miniature { get { return miniature; } set { miniature = value; } }

        #endregion

        #region Constructor

        public UserImage(string path, ImageFileType type)
        {
            this.path = path;
            this.type = type;
        }

        #endregion

        static public ImageFileType GetType(string extension)
        {
            switch(extension.ToLower())
            {
                case "jpg":
                    return ImageFileType.JPG;
                case "png":
                    return ImageFileType.PNG;
                default:
                    return ImageFileType.NOT_SUPPORTED;
            }
        }
    }
}
