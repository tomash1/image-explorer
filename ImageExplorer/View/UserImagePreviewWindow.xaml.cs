﻿using ImageExplorer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ImageExplorer.View
{
    /// <summary>
    /// Interaction logic for UserImagePreviewWindow.xaml
    /// </summary>
    public partial class UserImagePreviewWindow : Window
    {
        private UserImagePreviewViewModel viewModel;

        public UserImagePreviewWindow(UserImagePreviewViewModel viewModel)
        {
            InitializeComponent();
            this.viewModel = viewModel;
            DataContext = viewModel;
        }

        private void CloseWindow_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
