﻿using ImageExplorer.ViewModel;
using System.Windows;

namespace ImageExplorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            viewModel = new MainWindowViewModel();
            DataContext = viewModel;
        }

        private void OpenFolder_Click(object sender, RoutedEventArgs e)
        {
            viewModel.OpenFolderButtonClicked();
        }

        private void SamplePhoto_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            viewModel.ShowPhotoPreview(sender);
        }
    }
}
