﻿using ImageExplorer.Model;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageExplorer.Converter
{
    class UserImageToMiniatureConverter: IValueConverter
    {
        

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var photo = value as UserImage;
            if (photo == null)
                return null;
            if (photo.Miniature != null)
            {
                return photo.Miniature;
            }
            else
            {
                return "/ImageExplorer;component/Assets/EmptyMiniatureTemplate.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
