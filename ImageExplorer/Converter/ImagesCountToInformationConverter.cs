﻿using ImageExplorer.Model;
using ImageExplorer.Tools;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageExplorer.Converter
{
    class ImagesCountToInformationConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var count = (int) value;
            return "Found " + count + (count==1 ? " image" : " images") + " in folder";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

