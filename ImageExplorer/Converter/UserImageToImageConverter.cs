﻿using ImageExplorer.Model;
using ImageExplorer.Tools;
using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageExplorer.Converter
{
    class UserImageToImageConverter : IValueConverter
    {


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var photo = value as UserImage;
            if (photo == null)
                return null;
            var image = ImageManager.CreateBitmap(photo);
            if (image != null)
            {
                return image;
            }
            else
            {
                return "/ImageExplorer;component/Assets/EmptyMiniatureTemplate.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
