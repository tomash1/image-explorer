﻿using ImageExplorer.Model;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImageExplorer.Tools
{

    class PathLookup
    {

        private string path;

        public PathLookup(string pathToLookup)
        {
            this.path = pathToLookup;
        }
            
        public IEnumerable<UserImage> GetImages()
        {
            return findImagesInPath();
        }

        private IEnumerable<UserImage> findImagesInPath()
        {
            if (path != null && Directory.Exists(path))
            {
                return processDirectoryPath();
            }
            else
            {
                throw new IOException("Given path does not exists");
            }
        }

        private IEnumerable<UserImage> processDirectoryPath()
        {
            System.Object lockMe = new System.Object();
            string[] files = Directory.GetFiles(path);
            List<UserImage> userImages = new List<UserImage>();
            Parallel.ForEach<string>(files, file =>
            {
                var fileType = processFilePath(file);
                if (fileType != ImageFileType.NOT_SUPPORTED)
                {
                    var image = new UserImage(file, fileType);
                    lock(lockMe)
                    {
                        userImages.Add(image);
                    }
                }
            });
            return userImages;
        }

        private ImageFileType processFilePath(string path)
        {
            var split = path.Split('.');
            return UserImage.GetType(split.Last());
        }

    }
}
