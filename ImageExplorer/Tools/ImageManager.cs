﻿using ImageExplorer.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Media.Imaging;

namespace ImageExplorer.Tools
{
    class ImageManager
    {
        [DllImport("gdi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool DeleteObject(IntPtr value);

        #region Fields

        private string dirPath;
        private IEnumerable<UserImage> loadedImages;
        private UserImage[] foundImages;
        private int loadedCount = 0;
        private Thread generatorMainThread;
        List<Thread> generatorThreads = new List<Thread>();

        #endregion

        #region Properties

        public string DirectoryPath { get { return dirPath; } set { dirPath = value; } }
        public IEnumerable<UserImage> Images { get { return loadedImages != null ? loadedImages : Enumerable.Empty<UserImage>(); } }
        public int LoadedImagesCount { get { return loadedCount; } }

        #endregion

        public delegate void ImagesProcessed();

        public void LoadPhotos()
        {
            loadedCount = 0;
            loadedImages = null;
            var pl = new PathLookup(dirPath);
            loadedImages = pl.GetImages();
            loadedCount = loadedImages.Count();
        }

        public void GenerateMiniatures(ImagesProcessed callback)
        {
            if (generatorMainThread != null && generatorMainThread.IsAlive)
            {
                foreach(var t in generatorThreads)
                {
                    if (t != null && t.IsAlive)
                    {
                        t.Abort();
                    }
                }
                generatorMainThread.Abort();
            }
            generatorMainThread = new Thread( () => prepareGeneration(callback));
            generatorMainThread.Start();
        }

        private void prepareGeneration(ImagesProcessed callback)
        {
            foundImages = loadedImages.ToArray();
            if (foundImages.Length > 0)
            {
                var THREADS = Environment.ProcessorCount;
                var dataDivider = (int)foundImages.Length / THREADS;
                var over = foundImages.Length % THREADS;


                for (var i = 0; i < THREADS; i++)
                {
                    var firstIndex = dataDivider * i;
                    var lastIndex = firstIndex + dataDivider;
                    Thread thread;
                    if (i == THREADS - 1)
                    {
                        thread = new Thread(() => ThreadGenerationMethod(foundImages, firstIndex, lastIndex + over, callback));
                    }
                    else
                    {
                        thread = new Thread(() => ThreadGenerationMethod(foundImages, firstIndex, lastIndex, callback));
                    }
                    generatorThreads.Add(thread);
                    thread.Start();
                }
                foreach (var t in generatorThreads)
                {
                    t.Join();
                }
                callback();
            }
        }

        private void ThreadGenerationMethod(UserImage [] images, int firstIndex, int lastIndex, ImagesProcessed callback)
        {
            for(var i = firstIndex; i < lastIndex; i++)
            {
                generateUserImage(images[i]);
                if (i % 7 == 0)
                    callback();
            }
        }

        private void generateUserImage(UserImage im)
        {
            var miniature = Image.FromFile(im.Path).GetThumbnailImage(120, 120, () => false, IntPtr.Zero);
            if (miniature != null)
            {
                var bitmap = new Bitmap(miniature);
                IntPtr bmpPt = bitmap.GetHbitmap();
                BitmapSource bitmapSource =
                 System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(
                       bmpPt,
                       IntPtr.Zero,
                       Int32Rect.Empty,
                       BitmapSizeOptions.FromEmptyOptions());

                bitmapSource.Freeze();
                DeleteObject(bmpPt);

                im.Miniature = bitmapSource;
            }
        }

        public static BitmapImage CreateBitmap(UserImage userImage)
        {
            try
            {
                using (var fs = new FileStream(userImage.Path, FileMode.Open))
                {
                    var image = new BitmapImage();
                    image.BeginInit();
                    image.CacheOption = BitmapCacheOption.OnLoad;
                    image.StreamSource = fs;
                    image.EndInit();
                    image.Freeze();
                    return image;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
            return null;
        }

        
    }
}
 