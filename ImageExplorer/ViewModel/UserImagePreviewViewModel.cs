﻿using ImageExplorer.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageExplorer.ViewModel
{
    public class UserImagePreviewViewModel
    {

        #region Fields

        private UserImage userImage;

        #endregion

        #region Properties

        public UserImage Image { get { return userImage; } set { userImage = value; } }

        #endregion

        #region Constructor

        public UserImagePreviewViewModel()
        {
            
        }

        #endregion


    }
}
