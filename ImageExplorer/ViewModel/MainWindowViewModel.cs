﻿using ImageExplorer.Model;
using ImageExplorer.Tools;
using ImageExplorer.View;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Controls;


namespace ImageExplorer.ViewModel
{
    class MainWindowViewModel: INotifyPropertyChanged
    {

        #region Fields

        ImageManager manager;
        private int generatedImages = 0;

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Properties

        public ObservableCollection<UserImage> Images { get { return new ObservableCollection<UserImage>(manager.Images); } }
        public int ImagesCount { get { return manager.LoadedImagesCount; } }
        public int GeneratedImages { get { return generatedImages; } set { generatedImages = value; } }
        public bool ShowProgressBar { get { return ImagesCount > 0 && GeneratedImages < ImagesCount; } }

        #endregion

        #region Constructor

        public MainWindowViewModel()
        {
            manager = new ImageManager();
        }

        #endregion

        public void GeneratedImagesChanged()
        {
            if (this.PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("GeneratedImages"));
                PropertyChanged(this, new PropertyChangedEventArgs("ShowProgressBar"));
            }
        }

        public void ImageProccessingFinished()
        {
            generatedImages += 7;
            GeneratedImagesChanged();
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("Images"));
        }

        public void ChangedCountOfLoadedImages()
        {
            if (this.PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs("ImagesCount"));
        }

        public void OpenFolderButtonClicked()
        {
            try {
                generatedImages = 0;
                manager.DirectoryPath = getDirectoryPathFromOpenDialog();
                manager.LoadPhotos();
                ChangedCountOfLoadedImages();
                if (this.PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Images"));
                manager.GenerateMiniatures(ImageProccessingFinished);
                GeneratedImagesChanged();
            }
            catch
            {
                GeneratedImagesChanged();
            }
        }

        public void ShowPhotoPreview(object sender)
        {
            Button cmd = (Button)sender;
            if (cmd.DataContext is UserImage)
            {
                var vm = new UserImagePreviewViewModel();
                vm.Image = (UserImage)cmd.DataContext;
                var view = new UserImagePreviewWindow(vm);
                view.ShowDialog();
            }
        }

        private string getDirectoryPathFromOpenDialog()
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();

            System.Windows.Forms.DialogResult result = fbd.ShowDialog();

            if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
            {
                return fbd.SelectedPath;
            }
            return null;
        }
    }
}
